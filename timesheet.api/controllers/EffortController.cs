﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;
using timesheet.model;

namespace timesheet.api.controllers
{
    [Route("api/v1/effort")]
    [ApiController]
    public class EffortController : ControllerBase
    {
        private readonly IEffortService effortService;
        public EffortController(IEffortService effortService)
        {
            this.effortService = effortService;
        }

        [HttpGet("get/{id}")]
        public IActionResult GetEffort(string id)
        {
            var result = this.effortService. Geteffort(id);
            return new ObjectResult(result);
        }

        [HttpPost("addnew")]
        public IActionResult New([FromBody] Effort logeffort)
        {
            var result = this.effortService.new_effort(logeffort);
            return new ObjectResult(result);
        }
    }
}