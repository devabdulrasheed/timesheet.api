﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using timesheet.business;

namespace timesheet.api.controllers
{
    [Route("api/v1/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeService employeeService;
        public EmployeeController(IEmployeeService employeeService)
        {
            this.employeeService = employeeService;
        }

        [HttpGet("GetEmployeesWorkEffort")]
        public IActionResult GetEmployeesWorkEffortTotalandAvg()
        {
            var result = this.employeeService.GetEmployeesWorkEffortTotalandAvg();
            return new ObjectResult(result);
        }

        [HttpGet("getemployees")]
        public IActionResult GetEmployees()
        {
            var result = this.employeeService.GetEmployees();
            return new ObjectResult(result);
        }
    }
}