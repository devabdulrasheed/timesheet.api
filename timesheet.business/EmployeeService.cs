﻿using System;
using System.Linq;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EmployeeService : IEmployeeService
    {
        public TimesheetDb db { get; }
        public EmployeeService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<Employee> GetEmployees()
        {
            return this.db.Employees;
        }

        public IQueryable GetEmployeesWorkEffortTotalandAvg()
        {
            var result = from emp in this.db.Employees
                         join ef in this.db.Effort on emp.Code equals ef.EmployeeCode into e
                         from x in e.DefaultIfEmpty()
                         orderby emp.Id
                         select new
                         {
                             emp.Id,
                             emp.Code,
                             emp.Name,
                             totalwork = e.Sum(s => s.Saturday) + e.Sum(s => s.Monday) + e.Sum(s => s.Tuesday) + e.Sum(s => s.Wednesday) + e.Sum(s => s.Thursday) + e.Sum(s => s.Friday) + e.Sum(s => s.Saturday),
                             avgwork = (e.Sum(s => s.Saturday) + e.Sum(s => s.Monday) + e.Sum(s => s.Tuesday) + e.Sum(s => s.Wednesday) + e.Sum(s => s.Thursday) + e.Sum(s => s.Friday) + e.Sum(s => s.Saturday))/7
                         };
            return result;
        }

    }
}
