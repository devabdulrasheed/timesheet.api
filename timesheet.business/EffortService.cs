﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using timesheet.data;
using timesheet.model;

namespace timesheet.business
{
    public class EffortService : IEffortService
    {
        public TimesheetDb db { get; }
        public EffortService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }


        public IQueryable Geteffort(string empcode)
        {
            var result = from ef in this.db.Effort
                         join ts in this.db.Tasks on ef.TaskID equals ts.Id
                         where ef.EmployeeCode == empcode
                         select new
                         {
                             ts.Name,
                             ef.Sunday,
                             ef.Monday,
                             ef.Tuesday,
                             ef.Wednesday,
                             ef.Thursday,
                             ef.Friday,
                             ef.Saturday
                         };

            return result;
        }


        public IQueryable new_effort(Effort effort)
        {
            this.db.AddAsync(effort);

            this.db.SaveChangesAsync();
            return from ef in this.db.Effort
                   join ts in this.db.Tasks on ef.TaskID equals ts.Id
                   where ef.EmployeeCode == effort.EmployeeCode
                   select new
                   {
                       ts.Name,
                       ef.Sunday,
                       ef.Monday,
                       ef.Tuesday,
                       ef.Wednesday,
                       ef.Thursday,
                       ef.Friday,
                       ef.Saturday
                   };
        }
    }
}
