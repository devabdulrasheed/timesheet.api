﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace timesheet.data.Migrations
{
    public partial class Effort_Table : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Effort",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EmployeeCode = table.Column<string>(maxLength: 10, nullable: false),
                    TaskID = table.Column<int>(nullable: false),
                    Sunday = table.Column<decimal>(nullable: false),
                    Monday = table.Column<decimal>(nullable: false),
                    Tuesday = table.Column<decimal>(nullable: false),
                    Wednesday = table.Column<decimal>(nullable: false),
                    Thursday = table.Column<decimal>(nullable: false),
                    Friday = table.Column<decimal>(nullable: false),
                    Saturday = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Effort", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Effort");
        }
    }
}
