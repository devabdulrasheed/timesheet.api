﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class Effort
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [StringLength(10)]
        [Required]
        public string EmployeeCode { get; set; }

        [Required]
        public int TaskID { get; set; }

        [Required]
        public decimal Sunday { get; set; }

        [Required]
        public decimal Monday { get; set; }

        [Required]
        public decimal Tuesday { get; set; }

        [Required]
        public decimal Wednesday { get; set; }

        [Required]
        public decimal Thursday { get; set; }

        [Required]
        public decimal Friday { get; set; }

        [Required]
        public decimal Saturday { get; set; }
    }
}
